## 1.0.0-20201102
### Added
- Base on debian:buster-20200908-slim
- Add curl, jq, awscli, openssh-client
- Build docker 19.03.13 (learn from docker-library/docker)
